﻿using MovieSearch.Infrastructure;
using MovieSearch.Infrastructure.MovieProviders;
using MovieSearch.Models.MovieProviderModels.TMDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Models.ViewModels
{
  public class MoviesResultViewModel
  {
    public MovieProviderType Provider { get; set; }
    public List<Movie> Movies { get; set; } = new List<Movie>();
  }
}
