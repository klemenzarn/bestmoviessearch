﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Models.MovieProviderModels.TMDB
{
  public class Movie
  {
    [JsonProperty("id")]
    public int Id { get; set; }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("vote_average")]
    public double Rating { get; set; }

    [JsonProperty("overview")]
    public string Description { get; set; }

    [JsonProperty("vote_count")]
    public string VoteCount { get; set; }

    [JsonProperty("poster_path")]
    public string ImagePath { get; set; }

    [JsonProperty("genre_ids")]
    public List<int> GenreIds { get; set; } = new List<int>();

    public List<string> Genres { get; set; } = new List<string>();
  }
}
