﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Models.MovieProviderModels.TMDB
{
  public class GenreCollection
  {
    public List<Genre> genres { get; set; }
  }
}
