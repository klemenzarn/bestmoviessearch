﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Models.MovieProviderModels.TMDB
{
  public class MovieResults
  {
    [JsonProperty("total_results")]
    public int TotalResults { get; set; }

    [JsonProperty("total_pages")]
    public int TotalPages { get; set; }

    [JsonProperty("page")]
    public int Page { get; set; }

    [JsonProperty("results")]
    public List<Movie> Results { get; set; }
  }
}
