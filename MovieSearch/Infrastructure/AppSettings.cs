﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Infrastructure
{
  public class AppSettings : IAppSettings
  {
    public string TmdbApiKey { get; set; }
  }
}
