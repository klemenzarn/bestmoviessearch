﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Infrastructure
{
  public interface IAppSettings
  {
    string TmdbApiKey { get; set; }
  }
}
