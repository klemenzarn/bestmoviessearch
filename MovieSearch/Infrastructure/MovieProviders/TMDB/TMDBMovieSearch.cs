﻿using MovieSearch.Models.MovieProviderModels.TMDB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MovieSearch.Infrastructure.MovieProviders.TMDB
{
  public class TMDBMovieSearch : IMovieProvider
  {
    private string apiKey;


    public TMDBMovieSearch(string apiKey)
    {
      this.apiKey = apiKey;
    }

    public MovieProviderType Type => MovieProviderType.TMDB;


    public async Task<List<Movie>> SearchAsync(int year)
    {
      var genres = await Genres();

      // TODO: refactor
      string requestUrl = $"https://api.themoviedb.org/3/discover/movie?primary_release_date.gte={year}-01-11&primary_release_date.lte={year + 1}-01-01&api_key={apiKey}&sort_by=vote_average.desc&vote_count.gte=100";

      var client = new HttpClient();
      var searchResponse = await client.GetStringAsync(requestUrl);

      var movies = JsonConvert.DeserializeObject<MovieResults>(searchResponse);

      var pageCount = movies.TotalPages;

      for (int i = 2; i <= pageCount; i++)
      {
        requestUrl = $"https://api.themoviedb.org/3/discover/movie?primary_release_date.gte={year}-01-11&primary_release_date.lte={year + 1}-01-01&api_key={apiKey}&sort_by=vote_average.desc&vote_count.gte=100&page={i}";
        searchResponse = await client.GetStringAsync(requestUrl);
        var paginationResult = JsonConvert.DeserializeObject<MovieResults>(searchResponse);
        movies.Results.AddRange(paginationResult.Results);
      }

      foreach (var movie in movies.Results)
      {
        var movieGenres = genres.genres.Where(g => movie.GenreIds.Any(i => i == g.id)).Select(g => g.name);
        movie.Genres.AddRange(movieGenres);
      }

      return movies.Results;
    }

    public async Task<GenreCollection> Genres()
    {
      var client = new HttpClient();

      string genreRequestUrl = $"https://api.themoviedb.org/3/genre/movie/list?api_key={apiKey}&language=en-US";
      var genreResponse = await client.GetStringAsync(genreRequestUrl);
      var genres = JsonConvert.DeserializeObject<GenreCollection>(genreResponse);
      return genres;
    }

    public async Task<string> GetImdbUrlAsync(int id)
    {
      string requestUrl = $"https://api.themoviedb.org/3/movie/{id}?api_key={apiKey}&language=en-US";

      // TODO: refactor
      var client = new HttpClient();
      var searchResponse = await client.GetStringAsync(requestUrl);
      var movieDetails = JsonConvert.DeserializeObject<MovieDetails>(searchResponse);

      return $"https://www.imdb.com/title/{movieDetails.imdb_id}/";
    }
  }
}
