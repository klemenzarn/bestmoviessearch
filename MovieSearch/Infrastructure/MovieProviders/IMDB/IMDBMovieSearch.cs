﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieSearch.Models.MovieProviderModels.TMDB;

namespace MovieSearch.Infrastructure.MovieProviders.IMDB
{
  public class IMDBMovieSearch : IMovieProvider
  {
    public MovieProviderType Type => MovieProviderType.IMDB;

    public Task<List<Movie>> SearchAsync(int year)
    {
      // TODO: implementation for pipika :D see MovieSearch.Infrastructure.MovieProviders.TMDB.TMDBMovieSearch for example
      throw new NotImplementedException();
    }
  }
}
