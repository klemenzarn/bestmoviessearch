﻿using MovieSearch.Models.MovieProviderModels.TMDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Infrastructure.MovieProviders
{
  public interface IMovieProvider
  {
    MovieProviderType Type { get; }
    Task<List<Movie>> SearchAsync(int year);
  }
}
