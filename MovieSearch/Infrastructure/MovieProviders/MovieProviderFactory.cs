﻿using MovieSearch.Infrastructure.MovieProviders.IMDB;
using MovieSearch.Infrastructure.MovieProviders.TMDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieSearch.Infrastructure.MovieProviders
{
  public class MovieProviderFactory
  {
    private IAppSettings appSettings;

    public MovieProviderFactory(IAppSettings appSettings)
    {
      this.appSettings = appSettings;
    }

    public IMovieProvider Create(MovieProviderType movieProviderType)
    {
      switch (movieProviderType)
      {
        case MovieProviderType.TMDB:
          return new TMDBMovieSearch(appSettings.TmdbApiKey);
        case MovieProviderType.IMDB:
          return new IMDBMovieSearch();
      }

      throw new NotImplementedException($"Provider for {movieProviderType.ToString()} is not implemented yet.");
    }

  }
}
