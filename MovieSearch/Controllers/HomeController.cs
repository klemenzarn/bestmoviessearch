﻿using Microsoft.AspNetCore.Mvc;
using MovieSearch.Infrastructure;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using MovieSearch.Models.MovieProviderModels.TMDB;
using MovieSearch.Models.ViewModels;
using MovieSearch.Infrastructure.MovieProviders;
using MovieSearch.Infrastructure.MovieProviders.TMDB;

namespace MovieSearch.Controllers
{
  public class HomeController : Controller
  {
    private IMovieProvider movieSearchProvider;

    // TODO: gui, image, search, IMovieSearch interface, loči na serialization modele in return modele (uporabi mappera) ...
    // TODO: needs factory pattern and singelton for setting default movie provier

    public HomeController(MovieProviderFactory movieProviderFactory)
    {
      this.movieSearchProvider = movieProviderFactory.Create(MovieProviderType.TMDB); // TODO change to MovieProviderType.IMDB for IMDB search provider
    }

    public async Task<List<Movie>> Search(int? year)
    {
      return await movieSearchProvider.SearchAsync(year ?? DateTime.Now.Year);
    }

    public async Task<IActionResult> Index(int? year)
    {
      // todo slika: https://image.tmdb.org/t/p/w500{image}

      var movies = await Search(year);
      return View(new MoviesResultViewModel
      {
        Provider = movieSearchProvider.Type,
        Movies = movies
      });
    }

    public async Task<IActionResult> Imdb(int? id, MovieProviderType provider)
    {
      if (id == null)
      {
        return NotFound();
      }

      string imdbUrl = null;

      switch (provider)
      {
        case MovieProviderType.TMDB:
          if(movieSearchProvider is TMDBMovieSearch tmdbMovieSearch)
          {
            imdbUrl = await tmdbMovieSearch.GetImdbUrlAsync(id.Value);
          }
          break;
      }

      if(imdbUrl == null)
      {
        return NotFound();
      }
      else
      {
        return Redirect(imdbUrl);
      }
      
    }


  }
}
