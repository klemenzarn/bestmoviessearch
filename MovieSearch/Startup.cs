﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieSearch.Infrastructure;
using MovieSearch.Infrastructure.MovieProviders;

namespace MovieSearch
{
  public class Startup
  {
    private IConfiguration configuration;

    public Startup(IConfiguration configuration)
    {
      this.configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
      // register app settings
      AppSettings appSettings = new AppSettings();
      configuration.Bind(appSettings);
      services.AddSingleton<IAppSettings>(appSettings);

      // register factories
      services.AddTransient<MovieProviderFactory>();

      // register mvc
      services.AddMvc();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseMvcWithDefaultRoute();
    }
  }
}
